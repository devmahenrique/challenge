import styled, { css } from 'styled-components';

import FONTS from '../commons/Fonts';
import COLORS from '../commons/Colors';

export const ButtonCTA = styled.TouchableOpacity`
  margin-top: ${props => props.marginTop ? `${props.marginTop}px` : '0'};
  margin-bottom: ${props => props.marginBottom ? `${props.marginBottom}px` : '10px'};
  background-color: white;
  border-radius: 8px;
  background-color: ${props => props.secondary ? `${COLORS.secondary}` : `${COLORS.primary}`};

  ${props => props.active && css`background-color: ${COLORS.active}`};
  ${props => props.outline && css`
    border: 3px solid ${COLORS.secondary};
    background-color: transparent;
  `};
`;

export const Label = styled.Text`
  padding: 12px 10px;
  font-size: ${FONTS.buttonSize};
  text-align: center;
  color: ${props => props.secondary ? `${COLORS.base}` : 'white'};
`;
