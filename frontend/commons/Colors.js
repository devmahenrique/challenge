const COLORS = {
  base: '#333333',
  primary: '#00E1A3',
  secondary: '#E4E4E4',
  active: '#00CDB5'
}

export default COLORS;
