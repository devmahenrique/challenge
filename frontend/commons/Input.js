import styled from 'styled-components';

import FONTS from '../commons/Fonts';
import COLORS from '../commons/Colors';

export const Input = styled.TextInput`
  border-width: 1px;
  border-color: ${COLORS.secondary};
  border-top-width: 0;
  border-right-width: 0;
  border-left-width: 0;
  font-size: ${FONTS.baseSize};
  margin-bottom: 14px;
`;
