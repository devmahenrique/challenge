const FONTS = {
  baseSize: '16px',
  buttonSize: '17px',
  heading: '25px',
  smallSize: '14px'
}

export default FONTS;
