import React from 'react';
import { AppRegistry } from 'react-native';
import { ApolloProvider } from 'react-apollo';
import ApolloClient, { createNetworkInterface } from 'apollo-client';

import {name as appName} from './app.json';
import AppWithNavigationState from './scenes/App';

const networkInterface = createNetworkInterface({ uri: 'http://192.168.0.87:4000' });
const client = new ApolloClient({ networkInterface });

const App = () => (
  <ApolloProvider client={client}>
    <AppWithNavigationState />
  </ApolloProvider>
);

AppRegistry.registerComponent(appName, () => App);
