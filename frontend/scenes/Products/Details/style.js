import styled, { css } from 'styled-components';

import FONTS from '../../../commons/Fonts';
import COLORS from '../../../commons/Colors';

export const ScrollWrapper = styled.ScrollView`
  padding: 20px 10px 10px 10px;
  width: 100%;
  background-color: white;
`;

export const Box = styled.View`
  ${props => props.divider && css`
    padding-top: 15px;
    border: 1px solid ${COLORS.secondary};
    border-right-width: 0;
    border-left-width: 0;
    border-bottom-width: 0;
  `}
`;

export const Cover = styled.ImageBackground`
  margin-bottom: 15px;
  width: 100%;
  height: 280px;
  background-color: ${COLORS.secondary};
  border-radius: 8px;
`;

export const Title = styled.Text`
  margin-bottom: 15px;
  font-size: ${FONTS.heading};
  color: black;
`;

export const Price = styled.Text`
  margin-bottom: 15px;
  font-size: ${FONTS.buttonSize};
`;

export const Description = styled.Text`
  font-size: ${FONTS.baseSize};
  margin-bottom: 60px;
`;
