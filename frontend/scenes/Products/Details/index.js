import React, { Component } from 'react';
import { Text } from 'react-native';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import {
  ScrollWrapper,
  Box,
  Cover,
  Title,
  Price,
  Description
} from './style';

const DetailsQuery = gql`
{
  products {
    title,
    price,
    cover
  }
}`;

const ProductDetails = graphql(DetailsQuery)(({ data }) => {
  const { loading, products } = data;

  if (loading) return <Text>loading...</Text>;

  return (
    <ScrollWrapper>
      <Cover></Cover>
      <Title>Title Product</Title>
      <Price>R$69.99</Price>

      <Box divider>
        <Description>
          A cadeira de aproximação New é a peça que faltava para compor a decoração de seu escritório.
          É feita com estrutura em aço carbono com acabamento cromado.
          O assento e o encosto são produzidos em nylon dublado e revestidos em poliuretano na cor preta.
          Possui alça cromada na parte posterior do encosto e sapatas plásticas na base. Ideal para quem trabalha em escritórios ou home office e procura conforto e mobilidade. O modelo suporta até 130kg e tem medidas de 83cm de altura x 60cm de largura x 59cm de profundidade. Confira já!
          Tonalidades podem variar.
          Suporta ate 130kg
        </Description>
      </Box>
    </ScrollWrapper>
  );
});

export default ProductDetails;
