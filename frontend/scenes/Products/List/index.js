import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import { Text, TouchableOpacity } from 'react-native';

import { ButtonCTA, Label } from '../../../commons/Button';
import ListItem from './components/Item';

import { ScrollWrapper } from './style';

const ProductQuery = gql`
{
  products {
    id,
    title,
    price,
    cover
  }
}`;

const List = graphql(ProductQuery)(({ data }) => {
  const { loading, products } = data;

  if (loading) return <Text>loading...</Text>;

  return (
    <ScrollWrapper>
      {
        products.map((product) => (
          <TouchableOpacity key={product.id}>
            <ListItem data={product} />
          </TouchableOpacity>
        ))
      }
      <ButtonCTA marginTop={10} marginBottom={40}>
        <Label>Load More</Label>
      </ButtonCTA>
    </ScrollWrapper>
  );
});

export default List;
