import styled from 'styled-components';

import COLORS from '../../../../../commons/Colors';
import FONTS from '../../../../../commons/Fonts';

export const Item = styled.View`
  display: flex;
  flex-flow: row wrap;
  position: relative;
  margin-bottom: 15px;
  padding: 10px;
  width: 100%;
  height: 130px;
  background-color: white;
  border: 1px solid ${COLORS.secondary}
`;

export const Thumb = styled.ImageBackground`
  margin-right: 15px;
  width: 110px;
  height: 110px;
  background-color: ${COLORS.primary};
  border-radius: 8px;
`;

export const Content = styled.View`
  display: flex;
  flex-flow: column nowrap;
  width: 50%;
`;

export const Title = styled.Text`
  margin-bottom: 8px;
  font-size: ${FONTS.buttonSize};
`;

export const Price = styled.Text`
  font-size: ${FONTS.smallSize};
`;

export const SeeMore = styled.Text`
  position: absolute;
  bottom: 10px;
  right: 10px;
  font-size: ${FONTS.smallSize};
  color ${COLORS.secondary};
`;
