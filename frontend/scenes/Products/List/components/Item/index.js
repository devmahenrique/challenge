import React from 'react';

import {
  Item,
  Thumb,
  Content,
  Title,
  Price,
  SeeMore
} from './style';

const ListItem = ({data}) => (
  <Item>
    <Thumb source={{ uri: data.cover }} />

    <Content>
      <Title>{data.title}</Title>
      <Price>R${data.price}</Price>
    </Content>

    <SeeMore>+ see more</SeeMore>
  </Item>
);

export default ListItem;
