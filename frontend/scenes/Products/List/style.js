import styled from 'styled-components';

export const ScrollWrapper = styled.ScrollView`
  padding: 20px 10px 10px 10px;
  width: 100%;
  background-color: #e4e4e4;
`;
