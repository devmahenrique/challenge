import React, { Component } from 'react';

import { TouchableOpacity } from 'react-native';
import { ButtonCTA, Label } from '../../commons/Button';
import LoginForm from './components/login';
import RegisterForm from './components/register';

import { Wrapper, WrapperCta, CTA, ImageBG } from './style';

const cover = require('../../assets/cover.jpg');

class Auth extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
      isLogin: false,
      isRegister: false,
    };

    this.handleCover    = this.handleCover.bind(this);
    this.handleLogin    = this.handleLogin.bind(this);
    this.handleRegister = this.handleRegister.bind(this);
  }

  handleCover() {
    this.setState({
      isOpen: false,
      isLogin: false,
      isRegister: false
    });
  }

  handleLogin() {
    this.setState({
      isOpen: true,
      isLogin: true
    });
  }

  handleRegister() {
    this.setState({
      isOpen: true,
      isRegister: true
    });
  }

  render() {
    return (
      <Wrapper>
        <TouchableOpacity onPress={this.handleCover} activeOpacity={1}>
          <ImageBG source={cover} />
        </TouchableOpacity>

        <CTA open={this.state.isOpen}>
          { !this.state.isOpen &&
            <WrapperCta disabled={this.state.isOpen}>
              <ButtonCTA onPress={this.handleLogin}>
                <Label>Login</Label>
              </ButtonCTA>

              <ButtonCTA outline onPress={this.handleRegister}>
                <Label secondary>Register</Label>
              </ButtonCTA>
            </WrapperCta>
          }

          { this.state.isLogin &&
            <LoginForm closeArea={this.handleCover} />
          }

          { this.state.isRegister &&
            <RegisterForm closeArea={this.handleCover} />
          }
        </CTA>
      </Wrapper>
    );
  }
}

export default Auth;
