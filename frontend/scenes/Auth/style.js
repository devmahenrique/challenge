import styled from 'styled-components';

import COLORS from '../../commons/Colors';
import FONTS from '../../commons/Fonts';

export const Wrapper = styled.View`
  flex: 1;
`;

export const ImageBG = styled.ImageBackground`
  width: 100%;
  height: 100%;
  background-color: ${COLORS.secondary};
`;

export const CTA = styled.View`
  position: absolute;
  left: 0;
  bottom: 0;
  width: 100%;
  padding: 40px 30px;
  background-color: white;
`;

export const WrapperCta = styled.View`
  opacity: ${props => props.disabled ? '0.5': '1'};
`;

export const Form = styled.KeyboardAvoidingView`
  margin-top: 0;
`;

export const Heading = styled.Text`
  font-size: ${FONTS.heading};
  margin-bottom: 20px;
`;
