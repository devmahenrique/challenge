import React, { Component } from 'react';

import { ButtonCTA, Label } from '../../../commons/Button';
import { Input } from '../../../commons/Input';
import { Form, Heading } from '../style';

class RegisterForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name     : '',
      mail     : '',
      password : ''
    }

    this.onChangeName     = this.onChangeName.bind(this);
    this.onChangeEmail    = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
  }

  onChangeName(value) {
    this.setState({ name: value });
  }

  onChangeEmail(value) {
    this.setState({ mail: value });
  }

  onChangePassword(value) {
    this.setState({ password: value });
  }

  render() {
    return (
      <Form behavior="height" enabled>
        <Heading>
          Register
        </Heading>

        <Input
          placeholder='Name'
          onChangeText={this.onChangeName}
        />

        <Input
          placeholder='Email'
          onChangeText={this.onChangeEmail}
        />

        <Input
          placeholder='Password'
          onChangeText={this.onChangePassword}
          secureTextEntry
        />

        <ButtonCTA marginTop={20}>
          <Label>Continue</Label>
        </ButtonCTA>

        <ButtonCTA secondary marginTop={2} onPress={this.props.closeArea}>
          <Label secondary>Cancel</Label>
        </ButtonCTA>
      </Form>
    )
  }
}

export default RegisterForm;
