import React, { Component } from 'react';
import gql from 'graphql-tag';
import { ButtonCTA, Label } from '../../../commons/Button';
import { Input } from '../../../commons/Input';
import { Form, Heading } from '../style';
import { withNavigation } from 'react-navigation';

const USERS = gql`
{
  users {
    email,
    password
  }
}`;

class LoginForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mail     : '',
      password : '',
      navigation: this.props.navigation
    }

    this.onChangeEmail    = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.goToHome         = this.goToHome.bind(this);
  }

  onChangeEmail(value) {
    this.setState({ mail: value });
  }

  onChangePassword(value) {
    this.setState({ password: value });
  }

  goToHome() {
    // alert(this.props.navigation);
    // this.props.navigation.navigate('Home');
    // this.props.navigation('Home');
    // this.props.navigation.dispatch(
    //   NavigationActions.navigate({ routeName: "Login" })
    // );
    // this.props.navigation.openDrawer();
    // this.props.navigation.openDrawer();
  }

  render() {
    return (
      <Form behavior="height" enabled>
        <Heading>
          Login
        </Heading>

        <Input
          placeholder='Email'
          onChangeText={this.onChangeEmail}
        />

        <Input
          placeholder='Password'
          onChangeText={this.onChangePassword}
          secureTextEntry
        />

        <ButtonCTA marginTop={20}>
          <Label>Continue</Label>
        </ButtonCTA>

        <ButtonCTA secondary marginTop={2} onPress={this.props.closeArea}>
          <Label secondary>Cancel</Label>
        </ButtonCTA>
      </Form>
    );
  }
}

export default withNavigation(LoginForm);
// export default LoginForm;
