import React, { Component } from 'react';

import Routes from './Navigation';

class AppWithNavigationState extends Component {
  render() {
    return (
      <Routes navigation={this.props.navigation} />
    );
  }
}

export default AppWithNavigationState;
