import { createStackNavigator } from 'react-navigation';

import Auth from './Auth';
import ProductList from './Products/List';
import ProductDetails from './Products/Details';

const Routes = createStackNavigator(
  {
    Login   : { screen: Auth, navigationOptions: () => ({ header: null }) },
    Home    : { screen: ProductList, navigationOptions: () => ({ header: null }) },
    Details : { screen: ProductDetails },
  }, {
    initialRouteName: 'Login'
    // initialRouteName: 'Home'
    // initialRouteName: 'Details'
  }
);

export default Routes;
