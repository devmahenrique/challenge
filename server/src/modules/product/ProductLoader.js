export const loadAllProducts = (root, args, context) => {
    const products = [
      {
        id: 1,
        title: 'Cadeira Easy Azul',
        price: 69.99,
        cover: 'https://images.etna.com.br/produtos/43/412743/412743_detalhes.jpg',
        description: 'Product Cadeira Easy Azul description'
      },
      {
        id: 2,
        title: 'Cadeira Shell',
        price: 99.99,
        cover: 'https://images.etna.com.br/produtos/60/194160/194160_detalhes.jpg',
        description: 'Product Cadeira Shell description'
      },
      {
        id: 3,
        title: 'Cadeira Easy Laranja',
        price: 29.99,
        cover: 'https://images.etna.com.br/produtos/42/412742/412742_detalhes.jpg',
        description: 'Product Cadeira Easy Laranja description'
      },
      {
        id: 4,
        title: 'Cadeira Luigi',
        price: 299.99,
        cover: 'https://images.etna.com.br/produtos/38/414238/414238_detalhes.jpg',
        description: 'Product Cadeira Luigi description'
      },
      {
        id: 5,
        title: 'Cadeira Aproximação',
        price: 799.90,
        cover: 'https://images.etna.com.br/produtos/14/330014/330014_detalhes.jpg',
        description: 'Product Cadeira Aproximação description'
      },
    ];
  
    return products;
  };
  