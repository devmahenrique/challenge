import * as ProductLoader from './ProductLoader';

export const typeDefs = `
  type Product {
    id: ID
    title: String
    price: Float
    cover: String
    description: String
  }
`;

export const resolvers = {
  products: () => ProductLoader.loadAllProducts(),
};
