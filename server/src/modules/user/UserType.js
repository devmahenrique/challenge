import * as UserLoader from './UserLoader';

export const typeDefs = `
  type User {
    name: String
    email: String
    password: Int
  }
`;

export const resolvers = {
  users: () => UserLoader.loadAllUsers(),
};
